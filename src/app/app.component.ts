import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup ,Validators} from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  registrationForm!: FormGroup;
  constructor(){}

  ngOnInit(){
  this.registrationForm=new FormGroup({
      userName:new FormControl(null,Validators.required),
       password:new FormControl(null),
        confirmPassword:new FormControl(null),
        address:new FormGroup({
         city:new FormControl(null),
         state:new FormControl(null),
        postalCode:new FormControl(null)
    })
  });
}

//  registrationForm=new FormGroup({
//    userName:new FormControl('Ankita'),
//    password:new FormControl(''),
//    confirmPassword:new FormControl(''),
//     address:new FormGroup({
//      city:new FormControl(''),
//      state:new FormControl(''),
//     postalCode:new FormControl('')

//    })
//  });
 loadApiData(){
   this.registrationForm.patchValue({
    userName:'Ankita',
    password:'test',
    confirmPassword:'test',
     address:{
      city:'',
      state:'',
     postalCode:''
     }
   });
   
 }

 onSubmit(){
     console.log("Hello",this.registrationForm);
}
//  loadApiData(){
//   this.registrationForm.setValue({
//    userName:'Ankita',
//    password:'test',
//    confirmPassword:'test',
//     address:{
//      city:'City',
//      state:'State',
//     postalCode:'956942'
//     }
//   });
// }
}
